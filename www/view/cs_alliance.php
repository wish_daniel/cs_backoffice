<!-- this code is develop code for make new function -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />

<!-- login check -->
<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
    echo "<meta http-equiv='refresh' content='0;url=login.php'>";
    exit;
}
$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];
$user_privilige = $_SESSION['user_privilige'];
?>

<head>
<title>Wish_CS</title>
</head>
<body>
<header style="margin-left: 15px;">
<div>
    <div id="movingId" style="height:20px;margin-top:5px;">hello <? echo $user_id ?>. wishCS</div>
</div>

    <div style="display:inline-block; float: right; margin-top: 19px">
        <a href="http://www.wishtrends.net/~wish_cs_test/view/logout.php">Logout</a>
    <hr style="width: 100%; color: black; height: 3px; background-color:black;" />
    </div>

    <div class="bskr-title-ex">
        <h3 style="display: inline-block;margin-right: 10px;">Wish_CS</h3>
        <!-- 메뉴얼 보기_START -->
        <a class="btn btn-primary" data-toggle="collapse" href="#manual" aria-expanded="false" aria-controls="collapseExample" style="margin-top: -10px">
          Manual
        </a>

		<!-- go to wishtrend CS page -->
		<a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_main.php">
              WishtrendCS
        </a>

        <!-- Viewing user's CS list button -->
        <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_alliance.php">
            Partner shop CS
        </a>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_list.php">
              Admin
            </a>
        <? endif; ?>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_register.php">
              Register
            </a>
        <? endif; ?>

        <div class="collapse" id="manual">
            <div style="font-weight: 900; font-size: 20px; display: inline-table; text-align: top;">Menual</div>&nbsp;&nbsp;&nbsp;
                            
                  <? if($user_privilige == 0): ?>
                  <button type="button" id="edit" class="btn btn-default btn-xs" value="edit">Edit</button>
                  <button type="button" id="save" class="btn btn-default btn-xs" value="save">Save</button>
                  <? endif; ?>
                
                  <textarea class="form-control" id="edit_text" 
                  style="visibility: hidden; position: absolute; left: 20px;top: 90px; width:600px; height: 150px;"></textarea>         
            <div class="well" id="notice_main"></div>
        </div>  
        <!-- 메뉴얼 보기_END -->        
        <div class="clear"></div>        
    </div>    
    <div id="user_id"><? echo $user_id ?></div>
</header>

<!-- Container -->
<div class="container-fluid">
	<!-- input section -->
	<div>
		<div style="float:left;"> 
		    <div class="bskr-title-ex" style="width:200px; margin-left: 15px;">
		        <h4 id="test">Input Section</h4>
		        <div class="clear"></div>
		        <hr>
		    </div>     
		    
		    <table style="margin-bottom: 10px;">   
		        <tr>
		          <td>
		              <div class="form-group" style="margin-left:16px;">
		                <label for="InputEmail">E-Mail Account</label>                  
		                <input type="email" class="form-control" id="InputEmail" style="width:180px;">
		              </div>
		              <div class="form-group" style="margin-left: 15px;">
		                <label for="InputOrderRefText">Order Reference(Text)</label>
		                <input type="text" class="form-control" id="InputOrderRefText" style="width:180px;">
		              </div>
		              <div class="form-group" style="margin-left: 15px;">
		                <label for="InputOrderRefNum">Order Reference(Number)</label>
		                <input type="text" class="form-control" id="InputOrderRefNum" style="width:180px;">
		              </div>
		              <div class="form-group" style="margin-left: 15px;">
		                <label for="InputTransID">transaction ID</label>
		                <input type="text" class="form-control" id="InputTransID" style="width:180px;">
		              </div>
		              <div style="margin-left: 15px; margin-top: 7px;">
		                <button type="submit" class="btn btn-default" id="submit">Submit</button>
		              </div>
		          </td>                   
		     	</tr>
			</table>     
		</div>
	
		<div style="float:left; width:800px; margin-left: 30px;">
		    <!-- add cs section start -->    
		    <div class="bskr-title-ex">
		        <h4 id="test">Add CS Section</h4>
		        <div class="clear"></div>
		        <hr>
		    </div>  
		
		    <div class="form-group" style="display:inline-block;">
		        <label for="InputEmail">E-Mail</label>                  
		        <input type="email" class="form-control" id="cs_user_email" style="width:180px;">
		    </div>
		
		    <div class="form-group" style="display:inline-block;">
		        <label for="InputEmail">Content</label>                  
		        <input type="email" class="form-control" id="cs_content">
		    </div>
		
		    <button type="button" class="btn btn-default" id="save_alliance_cs" style="display:inline-block;">Save</button>
		    <!-- add cs section end -->
		</div>
	</div>

	<div style="display: block; width:800px; float:left; margin-left:30px;">
	    <!-- User CS Infomation -->
	    <div class="bskr-title-ex">
	        <h4 id="test">User CS Section</h4>            
	        <div class="clear"></div>
	        <hr>
	    </div>     
	    
	    <table class="table table-bordered" id="CSTable">
	        <thead>
	            <tr class="bg">                
	                <td class="col-md-2" align="center">Date</td>
	                <!-- <td class="col-md-2" align="center">Type</td> -->
	                <td class="col-md-7" align="center">Content</td>
	            </tr>
	        </thead>
	        <tbody>
	        </tbody>
	    </table>
    </div>
    
    <!-- Listing More CS button -->
    <div>    
        <div id="CSbutton" class="MoreStyle" data-toggle="collapse" href="#CS" aria-expanded="false" aria-controls="collapseExample">
            More CS
        </div>
    </div>

    <div class="collapse" id="CS">
        <!-- hidden cs table -->
        <table class="table table-bordered" id="CSHDTable">
            <thead>
                <tr class="bg">                
                    <td class="col-md-2" align="center">Date</td>                    
                    <td class="col-md-7" align="center">Content</td>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    
    
  </div>
    <!-- jquery 적용 -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <!-- daniel's script code -->
    <script type="text/javascript" src="../js/js_config/config.js"></script>
    <script type="text/javascript" src="../js/alliance/alliance_cs_control.js"></script>
    <!-- 트윈맥스 -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
    <script type="text/javascript" src="../js/circle.js"></script>
    <!-- 부트스트랩 사용하기위한 CDN -->
    <!-- 합쳐지고 최소화된 최신 CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>    
    <!-- daniel's css -->
    <link rel="stylesheet" href="../css/CS.css">
        
</body>
</html>    