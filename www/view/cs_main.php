<!-- this code is develop code for make new function -->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />

<!-- login check -->
<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
    echo "<meta http-equiv='refresh' content='0;url=login.php'>";
    exit;
}
$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];
$user_privilige = $_SESSION['user_privilige'];
?>

<head>
<title>Wish_CS</title>
</head>
<body>
<header style="margin-left: 15px;">
<div>
    <div id="movingId" style="height:20px;margin-top:5px;">hello <? echo $user_id ?>. wishCS</div>
</div>

    <div style="display:inline-block; float: right; margin-top: 19px">
        <a href="http://www.wishtrends.net/~wish_cs_test/view/logout.php">Logout</a>
    <hr style="width: 100%; color: black; height: 3px; background-color:black;" />
    </div>

    <div class="bskr-title-ex">
        <h3 style="display: inline-block;margin-right: 10px;">Wish_CS</h3>
        <!-- 메뉴얼 보기_START -->
        <a class="btn btn-primary" data-toggle="collapse" href="#manual" aria-expanded="false" aria-controls="collapseExample" style="margin-top: -10px">
          Manual
        </a>

		<!-- go to wishtrend CS page -->
		<a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_main.php">
              WishtrendCS
        </a>

        <!-- Viewing user's CS list button -->
        <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_alliance.php">
            Partner shop CS
        </a>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_list.php">
              Admin
            </a>
        <? endif; ?>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_register.php">
              Register
            </a>
        <? endif; ?>

        <div class="collapse" id="manual">
            <div style="font-weight: 900; font-size: 20px; display: inline-table; text-align: top;">Menual</div>&nbsp;&nbsp;&nbsp;
                            
                  <? if($user_privilige == 0): ?>
                  <button type="button" id="edit" class="btn btn-default btn-xs" value="edit">Edit</button>
                  <button type="button" id="save" class="btn btn-default btn-xs" value="save">Save</button>
                  <? endif; ?>
                
                  <textarea class="form-control" id="edit_text" 
                  style="visibility: hidden; position: absolute; left: 20px;top: 90px; width:600px; height: 150px;"></textarea>         
            <div class="well" id="notice_main"></div>
        </div>  
        <!-- 메뉴얼 보기_END -->        
        <div class="clear"></div>        
    </div>    
    <div id="user_id"><? echo $user_id ?></div>
</header>

<!-- Container -->
<div class="container-fluid">
    <!-- input section -->
    <div class="bskr-title-ex">
        <h4 id="test">Input Section</h4>
        <div class="clear"></div>
        <hr>
    </div>     
    
    <table>   
        <tr>
                  <td style="vertical-align: top;">
                      <div class="form-group">
                        <label for="InputEmail">Email/OrderNum/OrderRef</label>                  
                        <input type="email" class="form-control" id="SendInputBox" style="width:180px;">
                      </div>
                  </td>    
                  <td style="vertical-align: top;">
                      <div class="form-group" style="margin-left: 15px;">
                        <label for="InputTransID">transaction ID</label>
                        <input type="text" class="form-control" id="InputTransID" style="width:180px;">
                      </div>
                  </td>     
                  <td style="vertical-align: top;">                                        
                      <div style="margin-left: 15px; margin-top: 25px;">
                        <button type="submit" class="btn btn-default" id="submit">Submit</button>
                      </div>
                  </td>
                  <td>
                  	<table>
                  		<tr>
                  			<td>
		                  	    <div class="form-group" style="display:inline-block;margin-left: 50px;">
							        <label for="InputEmail">E-Mail</label>                  
							        <input type="email" class="form-control" id="cs_user_email" style="width:180px;">
							    </div>
							
							    <div class="form-group" style="display:inline-block;">
							        <label for="InputEmail">Content</label>                  
							        <input type="email" class="form-control" id="cs_content">
							    </div>
							
							    <button type="button" class="btn btn-default" id="save_cs" style="display:inline-block;">Save</button>                  				
                  			</td>
                  		</tr>
                  		<tr>
						    <table class="table table-bordered" id="CSTable" style="margin-left: 50px;width:744px;"> <!-- cs contents start -->
						        <thead>
						            <tr class="bg">                
						                <td class="col-md-2" align="center">Date</td>
						                <td class="col-md-7" align="center">Content</td>
						            </tr>
						        </thead>
						        <tbody>
						        </tbody>
						    </table> <!-- cs contents end -->
						    
						    <div> <!-- hidden cs contents start -->
						        <div id="CSbutton" class="MoreStyle" data-toggle="collapse" href="#CS" aria-expanded="false" aria-controls="collapseExample">
						            More CS
						        </div>
						    </div>
						
						    <div class="collapse" id="CS" style="margin-left: 50px;">
						        <table class="table table-bordered" id="CSHDTable">
						            <thead>
						                <tr class="bg">                
						                    <td class="col-md-2" align="center">Date</td>                    
						                    <td class="col-md-7" align="center">Content</td>
						                </tr>
						            </thead>
						            <tbody>
						            </tbody>
						        </table>
						    </div> <!-- hidden cs contents end -->
						    
                  		</tr>
                  	</table>
                  </td> 
     </tr>
     </table>     
     <!-- input section end -->    
     
    <!-- Account Section 시작 -->
    <div style="height:10px;"></div>            
    <div class="bskr-title-ex">
        <h4 style="display:inline-block">Account Section</h4>
        <div class="clear"></div>
        <hr>
    </div>      
    <table class="table table-bordered">
        <thead>
            <tr class="bg">
                <td>
                 E-mail account <button id="active"></p>                                                     
                </td>
            </tr>                
        </thead>
        <tbody>
            <tr>
                <td>
                 <div id="td_email"></div>
                </td>
            </tr>
        </tbody>
    </table>        
    
    <table class="table table-bordered" id="UserTable">
        <thead>
            <tr class="bg">
                <td class="col-md-1" align="center">Order Reference(Num)</td>
                <td class="col-md-1" align="center">Order Reference(Text)</td>
                <td class="col-md-1" align="center">Ordered Date</td>
                <td class="col-md-1" align="center">Shipped Date</td>
                <td class="col-md-1" align="center">Tracking Code</td>
                <td class="col-md-1" align="center">Order Cancell</td>
            </tr>
        </thead>
        <tbody>          
        </tbody>
    </table>
     <!-- Listing More Order button -->
     <div>
        <div id="Orderbutton" class="MoreStyle" data-toggle="collapse" href="#UserHD" aria-expanded="false" aria-controls="collapseExample">
                More Orders
        </div>
    </div>

    <div class="collapse" id="UserHD">
        <!-- hidden Usertable -->
        <table class="table table-bordered" id="UserHDTable">
            <thead>
                <tr class="bg">
                    <td class="col-md-1" align="center">Order Reference(Num)</td>
                    <td class="col-md-1" align="center">Order Reference(Text)</td>
                    <td class="col-md-1" align="center">Ordered Date</td>
                    <td class="col-md-1" align="center">Shipped Date</td>
                    <td class="col-md-1" align="center">Tracking Code</td>
                    <td class="col-md-1" align="center">Order Cancell</td>
                </tr>
            </thead>
            <tbody>          
            </tbody>
        </table>
    </div>    

    <div style="height:20px;"></div>                 
                  
    <!-- Order Section 시작 -->
    <div class="bskr-title-ex">
        <h4>Order Section</h4>
        <div class="clear"></div>
        <hr>
    </div>      
    
    <!-- OrderRefText(reference) - OrderRefNum(id_order) - TransactionID -->
    <table data-toggle="table" class="table table-bordered">
        <thead>
            <tr class="bg">
                <td>
                 OrderReference(Text)                                                     
                </td>
                <td>
                 OrderReference(Num)                                                     
                </td>
                <td>
                 Transaction ID                                                     
                </td>
            </tr>                
        </thead>
        <tbody>
            <tr>
                <td>
                 <div id="td_OrderRefText_1"></div>
                </td>
                <td>
                 <div id="td_OrderRefNum_1"></div>
                </td>
                <td>
                 <div id="td_transactionID"></div>
                </td>
            </tr>
        </tbody>
    </table>   
    
    <table class="table table-bordered" id="AddressTable">
        <thead>
            <tr class="bg">
                <td>
                 Address 
                    <!-- address change modal -->
                    <button class="btn btn-default" data-target="#addresspop" data-toggle="modal" id="change_address_modal" style="height:25px; width:140px; line-height: 10px; margin-top:-3px;">
                        Address Change
                    </button>
                    <br/>
                    <div class="modal fade" id="addresspop" >
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <!-- header -->
                          <div class="modal-header">
                            <!-- 닫기(x) 버튼 -->
                            <button type="button" class="close" data-dismiss="modal">×</button>
                            <!-- header title -->
                            <h4 class="modal-title">Address Change</h4>
                            <div id="ErrorDisplay"></div>
                          </div>
                              <!-- body -->
                              <div class="modal-body">
                                <label for="Address 1">Address 1</label>                  
                                <input type="text" class="form-control" id="change_address_1" style="width:400px;">      
                                <label for="Address 2">Address 2</label>                  
                                <input type="text" class="form-control" id="change_address_2" style="width:400px;">      
                                <label for="postcode">postcode</label>                  
                                <input type="text" class="form-control" id="change_postcode" style="width:180px;">      
                                <label for="city">city</label>                  
                                <input type="text" class="form-control" id="change_city" style="width:180px;">      
                                <label for="state">state</label>                  
                                <select id="change_state" class="form-control" style="width:200px;">
                                </select>     
                                <label for="phone">phone</label>                  
                                <input type="text" class="form-control" id="change_phone" style="width:180px;">      
                                <label for="phone_mobile">phone_mobile</label>                  
                                <input type="text" class="form-control" id="change_phone_mobile" style="width:180px;">      
                              </div>
                              <!-- Footer -->
                              <div class="modal-footer">
                                <!-- <button type="button" class="btn btn-default" data-dismiss="modal" id="save_address_modal">Save</button> -->
                                <button type="button" class="btn btn-default" id="save_address_modal">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" id="close_address_modal">Close</button>
                              </div>
                              
                        </div>
                      </div>
                    </div>
                </td>                
            </tr>                
        </thead>
        <tbody>            
        </tbody>
    </table>   
    


    <!-- ProductName - ProductQuantity - ProductPrice - TotalPrice -->
    <!-- <table class="table table-bordered" id="ProductTable"> -->
    <table class="table table-bordered" id="ProductTable">
        <thead>
            <tr class="bg">
                <td align="center">
                 ProductName                                                     
                </td>
                <td align="center">
                 ProductQuantity                                                     
                </td>
                <td align="center">
                 ProductPrice                                                     
                </td>
                <td align="center">
                 TotalPrice                                                     
                </td>              
            </tr>                
        </thead>
        <tbody>                             
        </tbody>
    </table>
    
    <!-- Voucher - DiscountPrice -->
    <table class="table table-bordered" id="DiscountTable">
    <!-- <table id="DiscountTable"> -->
        <thead>
            <tr class="bg">
                <td align="center">
                 Voucher                                                     
                </td>
                <td align="center">
                 DiscountPrice                                                     
                </td>
                <td align="center">
                 ShipingPrice                                                     
                </td>
            </tr>                
        </thead>
        <tbody>            
        </tbody>
    </table> 

    <!-- TotalPrice -->
    <table class="table table-bordered" id="TotalTable" >
    <!-- <table id="TotalTable" >     -->
        <thead>
            <tr class="bg">
                <td align="center">
                 Total Price                                 
                </td>                
            </tr>                
        </thead>
        <tbody>            
        </tbody>
    </table> 

    <!-- History -->
    <table class="table table-bordered" id="History">
        <thead>
            <tr class="bg">
                <td align="center">
                 State                                                     
                </td>
                <td align="center">
                 first name                                                     
                </td>
                <td align="center">
                 last name                                                     
                </td>
                <td align="center">
                 Do date                                                     
                </td>              
            </tr>                
        </thead>
        <tbody>                             
        </tbody>
    </table>

<!-- Container --></div>    

    <!-- jquery 적용 -->
    <script src="../js/jquery-1.11.3.min.js"></script>
    <!-- daniel's script code -->
    <script type="text/javascript" src="../js/OrderInfo.js"></script>
    <!-- 합쳐지고 최소화된 최신 CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <!-- 부가적인 테마 -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    
    <!-- 합쳐지고 최소화된 최신 자바스크립트 -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <!-- daniel's css -->
    <link rel="stylesheet" href="../css/CS.css">
        
</body>
</html>