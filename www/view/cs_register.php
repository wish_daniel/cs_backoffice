<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />

<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
    echo "<meta http-equiv='refresh' content='0;url=login.php'>";
    exit;
}
$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];
$user_privilige = $_SESSION['user_privilige'];
?>

<head>
	<title>CS List</title>
</head>
<body>
<header style="margin-left: 15px;">
<div>
    <div id="movingId" style="height:20px;margin-top:5px;">hello <? echo $user_id ?>. wishCS</div>
</div>

    <div style="display:inline-block; float: right; margin-top: 19px">
        <a href="http://www.wishtrends.net/~wish_cs_test/view/logout.php">Logout</a>
    <hr style="width: 100%; color: black; height: 3px; background-color:black;" />
    </div>

    <div class="bskr-title-ex">
        <h3 style="display: inline-block;margin-right: 10px;">Wish_CS</h3>
        <!-- 메뉴얼 보기_START -->
        <a class="btn btn-primary" data-toggle="collapse" href="#manual" aria-expanded="false" aria-controls="collapseExample" style="margin-top: -10px">
          Manual
        </a>

		<!-- go to wishtrend CS page -->
		<a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_main.php">
              WishtrendCS
        </a>

        <!-- Viewing user's CS list button -->
        <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_alliance.php">
            Partner shop CS
        </a>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_list.php">
              Admin
            </a>
        <? endif; ?>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_register.php">
              Register
            </a>
        <? endif; ?>

        <div class="collapse" id="manual">
            <div style="font-weight: 900; font-size: 20px; display: inline-table; text-align: top;">Menual</div>&nbsp;&nbsp;&nbsp;
                            
                  <? if($user_privilige == 0): ?>
                  <button type="button" id="edit" class="btn btn-default btn-xs" value="edit">Edit</button>
                  <button type="button" id="save" class="btn btn-default btn-xs" value="save">Save</button>
                  <? endif; ?>
                
                  <textarea class="form-control" id="edit_text" 
                  style="visibility: hidden; position: absolute; left: 20px;top: 90px; width:600px; height: 150px;"></textarea>         
            <div class="well" id="notice_main"></div>
        </div>  
        <!-- 메뉴얼 보기_END -->        
        <div class="clear"></div>        
    </div>    
    <div id="user_id"><? echo $user_id ?></div>
</header>

<div class="container">
    <h1 class="well">Registration Form</h1>
	<div class="col-lg-12 well">
	<div class="row">
		<form method='post' action='http://www.wishtrends.net/~wish_cs/controller/cs_register.php'>
			<div class="col-sm-12">
				<div class="row">
					<div class="col-sm-6 form-group">
						<label>Email</label>
						<input type="text" placeholder="User's Email" class="form-control" name="user_id">
					</div>
					<div class="form-group col-sm-6">
						<label>Password</label>
					<input type="password" placeholder="User's Password" class="form-control" name="user_pw">
				</div>							
				</div>					
				<div class="row">
					<div class="col-sm-6 form-group">
						<label>Name</label>
						<input type="text" placeholder="User's Name" class="form-control" name="user_name">
					</div>
					<div class="form-group col-sm-6">
						<label>Privilege</label>
					<input type="text" placeholder="User's Privilege" class="form-control" name="user_pri">
				</div>
				</div>
				
				<input class="btn btn-lg btn-info" type="submit" value="Submit">					
			</div>
		</form> 
		</div>
	</div>
</div>


<!-- daniel's script code -->
<script type="text/javascript" src="../js/js_config/config.js"></script>
<!-- daniel's css -->
<link rel="stylesheet" href="../css/register.css">

<!-- jqeury -->
<script src="../js/jquery-1.11.3.min.js"></script>
<!-- 부트스트랩 사용하기위한 CDN -->
<!-- 합쳐지고 최소화된 최신 CSS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    


</body>
</html>