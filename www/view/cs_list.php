<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />

<?php
session_start();
if(!isset($_SESSION['user_id']) || !isset($_SESSION['user_name'])) {
    echo "<meta http-equiv='refresh' content='0;url=login.php'>";
    exit;
}
$user_id = $_SESSION['user_id'];
$user_name = $_SESSION['user_name'];
$user_privilige = $_SESSION['user_privilige'];
?>

<head>
	<title>CS List</title>
</head>
<body>
<header style="margin-left: 15px;">
<div>
    <div id="movingId" style="height:20px;margin-top:5px;">hello <? echo $user_id ?>. wishCS</div>
</div>

    <div style="display:inline-block; float: right; margin-top: 19px">
        <a href="http://www.wishtrends.net/~wish_cs_test/view/logout.php">Logout</a>
    <hr style="width: 100%; color: black; height: 3px; background-color:black;" />
    </div>

    <div class="bskr-title-ex">
        <h3 style="display: inline-block;margin-right: 10px;">Wish_CS</h3>
        <!-- 메뉴얼 보기_START -->
        <a class="btn btn-primary" data-toggle="collapse" href="#manual" aria-expanded="false" aria-controls="collapseExample" style="margin-top: -10px">
          Manual
        </a>

		<!-- go to wishtrend CS page -->
		<a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_main.php">
              WishtrendCS
        </a>

        <!-- Viewing user's CS list button -->
        <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_alliance.php">
            Partner shop CS
        </a>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_list.php">
              Admin
            </a>
        <? endif; ?>
        <? if($user_privilige == 0): ?>
            <a class="btn btn-primary" style="margin-top: -10px" href="http://www.wishtrends.net/~wish_cs/view/cs_register.php">
              Register
            </a>
        <? endif; ?>

        <div class="collapse" id="manual">
            <div style="font-weight: 900; font-size: 20px; display: inline-table; text-align: top;">Menual</div>&nbsp;&nbsp;&nbsp;
                            
                  <? if($user_privilige == 0): ?>
                  <button type="button" id="edit" class="btn btn-default btn-xs" value="edit">Edit</button>
                  <button type="button" id="save" class="btn btn-default btn-xs" value="save">Save</button>
                  <? endif; ?>
                
                  <textarea class="form-control" id="edit_text" 
                  style="visibility: hidden; position: absolute; left: 20px;top: 90px; width:600px; height: 150px;"></textarea>         
            <div class="well" id="notice_main"></div>
        </div>  
        <!-- 메뉴얼 보기_END -->        
        <div class="clear"></div>        
    </div>    
    <div id="user_id"><? echo $user_id ?></div>
</header>	

	<div class="CsBackBtn">
		<div style="display: inline-block;">
			<div id="tabnum_1" style="display:inline-block;">
				<a href="#Tab_CSList" class="btn btn-primary" controls="Tab_CSList" role="tab" data-toggle="tab" style="width:100px; display:inline-block;">CS List</a>
			</div>
			<div style="display:inline-block;">
				<a id="btn" class="btn btn-primary" style="width:115px;">CS List Down</a>
			</div>
		</div>
		<div style="display: inline-block; margin-left: 20px;">
			<div id="tabnum_2" style="display:inline-block;">
				<a href="#Tab_PartnerCSList" class="btn btn-primary" controls="Tab_PartnerCSList" role="tab" data-toggle="tab" style="width:150px; display:inline-block;">Partnershop CS List</a>
			</div>
			<div style="display:inline-block;">
				<a id="BtnPartner" class="btn btn-primary" style="width:190px;">Partnershop CS List Down</a>
			</div>
		</div>
		<div style="display: inline-block; margin-left: 20px;">
			<div style="display:inline-block;">
				<a href="#Tab_CancelList" class="btn btn-primary" controls="Tab_CancelList" role="tab" data-toggle="tab" style="width:100px;">Cancel List</a>
			</div>
			<!-- <div style="display:inline-block;">
				<a id="data_extract" class="btn btn-primary" style="width:100%;">CS data extract</a>
			</div> -->
		</div>
	</div>

	<!-- CS List table -->
	<div id="" class="tab-content" style="margin-top:37px;">
		<div role="tabpanel" class="tab-pane" id="Tab_CSList">
			<div class="form-row show-inputbtns" style="margin-left: 20px; display: inline-block;">
			    Start Date : <input id="Sdate" type="date" data-date-inline-picker="true" data-date-open-on-focus="true" />
			</div>
			<div class="form-row show-inputbtns" style="margin-left: 10px; display: inline-block;">
			    End Date : <input id="Edate" type="date" data-date-inline-picker="true" data-date-open-on-focus="true" />
			</div>
			<a id="CalBtn" class="btn btn-primary">
			              Search
			</a>

			<div style="width:2248px;">
				<div class="bskr-title-ex CsListTable">
					<table data-toggle="table" class="table table-bordered" id="IdCsListTable">
					<!-- <table data-toggle="table" class="table table-bordered" id="tblExport"> -->
						<thead>
							<tr>
								<td align="center" colspan="6" class="table_middle"><span style="font-size: 24px">CS List</span></td>
							</tr>
							<tr>
								<td align="center" class="table_middle">
									CS Index
								</td>
								<td align="center" class="table_middle">
									Employee
								</td>
								<td align="center" class="table_middle">
									Email
								</td>
								<td align="center" class="table_middle">
									Enroll Date 
								</td>
								<td align="center" class="table_middle">
									Type
								</td>
								<td align="center" class="table_middle">
									Content
								</td>
							</tr>
						</thead>
						<tbody>			
						</tbody>
					</table>
				</div>
			</div>
		</div>

	<!-- Partnershop CS List table -->
		<div role="tabpanel" class="tab-pane" id="Tab_PartnerCSList">
			<!-- calendar start -->
			<div class="form-row show-inputbtns" style="margin-left: 20px; display: inline-block;">
			    Start Date : <input id="SdatePartner" type="date" data-date-inline-picker="true" data-date-open-on-focus="true" />
			</div>
			<div class="form-row show-inputbtns" style="margin-left: 10px; display: inline-block;">
			    End Date : <input id="EdatePartner" type="date" data-date-inline-picker="true" data-date-open-on-focus="true" />
			</div>
			<a id="CalBtnPartner" class="btn btn-primary">
			              Search
			</a>
			<!-- calendar start -->

			<div style="width:2248px;">
				<div class="bskr-title-ex CsListTable">
					<table data-toggle="table" class="table table-bordered" id="IdCsListTablePartner">
						<thead>
							<tr>
								<td align="center" colspan="6" class="table_middle"><span style="font-size: 24px">CS List</span></td>
							</tr>
							<tr>
								<td align="center" class="table_middle">
									CS Index
								</td>
								<td align="center" class="table_middle">
									Employee
								</td>
								<td align="center" class="table_middle">
									Email
								</td>
								<td align="center" class="table_middle">
									Enroll Date 
								</td>
								<td align="center" class="table_middle">
									Type
								</td>
								<td align="center" class="table_middle">
									Content
								</td>
							</tr>
						</thead>
						<tbody>			
						</tbody>
					</table>
				</div>
			</div>
		</div>

	<!-- cancellation table -->
		<div role="tabpanel" class="tab-pane" id="Tab_CancelList">
			<div class="bskr-title-ex CancellTable">
				<table data-toggle="table" class="table table-bordered" id="IdCancellTable">
					<thead>
						<tr>
							<td align="center" colspan="4"><span style="font-size: 24px">Cancellation List</span></td>
							<td align="center" colspan="5">
								<select id="search_cancel_type" class="form-control">
									<option value="email">email</opotion>
									<option value="order_num">OrderReferenceNum</opotion>
									<option value="order_text">OrderReferenceText</opotion>
									<option value="trance_id">TransactionID</opotion>
								</select>
								<input type="text" id="search_cancel_text" class="form-control"> 
								<a id="CancelSearchBtn" class="btn btn-primary">
		              				Search
		    					</a>
		    					<a id="CancelReset" class="btn btn-primary">
		              				Reset
		    					</a>
							</td>
						</tr>
						<tr>
							<td align="center" class="table_middle">
								Check Box
							</td>
							<td align="center" class="table_middle">
								Cancell Index
							</td>
							<td align="center" class="table_middle">
								Enroll Date
							</td>
							<td align="center" style="width:180px;" class="table_middle">
								<div style="float: left; margin-top: 8px; margin-left: 9px;">
								State
								</div>
								<select id="StateSelectorTop" class="form-control">
									<option>All</opotion>
									<option>Untreated</opotion>
									<option>Process</opotion>
									<option>Complete</opotion>
								</select>
							</td>
							<td align="center" class="table_middle">
								Email
							</td>
							<td align="center" class="table_middle">
								Order Reference(text)
							</td>
							<td align="center" class="table_middle">
								Order Reference(num)
							</td>
							<td align="center" class="table_middle">
								Transaction ID
							</td>
							<td align="center" class="table_middle">
								Total Price
							</td>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
				<div class="bskr-title-ex CancellTable">
					<a id="CancelStateSaveBtn" class="btn btn-primary">
				              State Save
				    </a>
					<select id="StateSelector" class="form-control">
						<option>Untreated</opotion>
						<option>Process</opotion>
						<option>Complete</opotion>
					</select>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- jquery 적용 -->
<script src="../js/jquery-1.11.3.min.js"></script>
<!-- daniel's script code -->
<script type="text/javascript" src="../js/CallCSList.js"></script>
<script type="text/javascript" src="../js/CallCancellList.js"></script>
<script type="text/javascript" src="../js/CancelListSearch.js"></script>
<script type="text/javascript" src="../js/StateChecker.js"></script>
<script type="text/javascript" src="../js/Calendar.js"></script>
<script type="text/javascript" src="../js/CSListDown.js"></script>
<script type="text/javascript" src="../js/ExcelDownload.js"></script>
<script type="text/javascript" src="../js/cs_data_excel.js"></script>

<!-- 부트스트랩 사용하기위한 CDN -->
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- daniel's css -->
<link rel="stylesheet" href="../css/CS.css">

</body>
</html>