<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" 
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko"  xml:lang="ko">
<meta http-equiv="Content-Type" Content="text/html; charset=utf-8" />
<head>
	<title>CS List</title>
</head>
<body>
<header>
	<div style="display: inline-block;">
		<a id="CsBackBtn" class="btn btn-primary" href="http://www.wishtrends.net/~wish_cs/view/cs_main.php">
              Main
        </a>
	</div>
</header>

<div>
	<!-- structure -->
</div>

<!-- jquery 적용 -->
<script src="../js/jquery-1.11.3.min.js"></script>
<!-- 부트스트랩 사용하기위한 CDN -->
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
<!-- 부가적인 테마 -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">    
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<!-- daniel's css -->
<link rel="stylesheet" href="../css/CS.css">

<div class="Wrapper">
	<div class="bskr-title-ex CsListTable">
		<table data-toggle="table" class="table table-bordered" id="IdCsListTable">
			<thead>
				<tr>
					<td align="center" colspan="2" class="table_middle"><span style="font-size: 24px">Count List</span></td>
				</tr>
				<tr>
					<td align="center" class="table_middle">
						admin ID
					</td>
					<td align="center" class="table_middle">
						Count
					</td>
				</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>
</div>

<script type="text/javascript" src="../js/admin_statistics.js"></script>

</body>
</html>