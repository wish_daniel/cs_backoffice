$(document).ready(function(){

    function itoStr($num)
    {
        $num < 10 ? $num = '0'+$num : $num;
        return $num.toString();
    }

    var btn = $('#btn');
    var tbl = 'IdCsListTable';

    btn.click(function(e){

        var dt = new Date();
        var year =  itoStr( dt.getFullYear() );
        var month = itoStr( dt.getMonth() + 1 );
        var day =   itoStr( dt.getDate() );
        var hour =  itoStr( dt.getHours() );
        var mins =  itoStr( dt.getMinutes() );

        var postfix = year + month + day + "_" + hour + mins;
        var fileName = "MyTable_"+ postfix + ".xls";

        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById( tbl );
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        a.href = data_type + ', ' + table_html;
        a.download = fileName;
        a.click();

        e.preventDefault();
    });
});


$(document).ready(function(){

    function itoStr($num)
    {
        $num < 10 ? $num = '0'+$num : $num;
        return $num.toString();
    }

    var btn = $('#BtnPartner');
    var tbl = 'IdCsListTablePartner';

    btn.click(function(e){

        var dt = new Date();
        var year =  itoStr( dt.getFullYear() );
        var month = itoStr( dt.getMonth() + 1 );
        var day =   itoStr( dt.getDate() );
        var hour =  itoStr( dt.getHours() );
        var mins =  itoStr( dt.getMinutes() );

        var postfix = year + month + day + "_" + hour + mins;
        var fileName = "MyTable_"+ postfix + ".xls";
		
        var a = document.createElement('a');
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById( tbl );
        
        console.log(table_div);
        
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        a.href = data_type + ', ' + table_html;
        a.download = fileName;
        a.click();

        e.preventDefault();
    });
});