// Add CS History
$(document).ready(function(){ 
    $('#save_alliance_cs').click(function(e){
        var chk = 'alliance_insert';
        var email = $('#cs_user_email').val();
        var content = $('#cs_content').val();
        var user_id = $('#user_id').html();
        $('#cs_content').val('');

        //CS contents regulation
        var change_str_content = content.replace(/'/g, "''");

        $.ajax({
            url: url_variable.wishtrends_url+'controller/db_control.php',
            type:'POST',
            dataType:'jsonp',
            crossDomain:true,
            data:{
                'chk':chk,
                'email':email,
                'content':change_str_content,
                'user_id':user_id,
            },
            success:function(data){
            	console.log(data);
                $('#CSTable > tbody').html('');
                $('#CSHDTable > tbody').html('');
                var user_cs = data;

                if(user_cs[0][2] == null)
                {
                    $('#CSbutton').css('visibility','hidden');
                    $('#Orderbutton').css('visibility','hidden');
                } else {
                    $('#CSbutton').css('visibility','visible');
                }

                $n=0;                        
                while($n != 2){                            
                    $('#CSTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;
                }

                //hidden CS table control
                $n=2;                        
                while(user_cs[0]!=null && user_cs[0][$n]!=null){                            
                    $('#CSHDTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;
                }
            }
        })
    });
});

// search user cs list
$(document).ready(function(){ 
    $('#submit').click(function(e){
        var chk = 'alliance_search';
        var email = $('#InputEmail').val();        


        $.ajax({
            url: url_variable.wishtrends_url+'controller/db_control.php',
            type:'POST',
            dataType:'jsonp',
            crossDomain:true,
            data:{
                'chk':chk,
                'email':email,
            },
            success:function(data){
            	console.log(data);
            	$('#CSTable > tbody').html('');
                $('#CSHDTable > tbody').html('');
                var user_cs = data;

                if(user_cs[0][2] == null)
                {
                    $('#CSbutton').css('visibility','hidden');
                    $('#Orderbutton').css('visibility','hidden');
                } else {
                    $('#CSbutton').css('visibility','visible');
                }

                $n=0;                        
                while($n != 2){                            
                    $('#CSTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;
                }

                //hidden CS table control
                $n=2;                        
                while(user_cs[0]!=null && user_cs[0][$n]!=null){                            
                    $('#CSHDTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;           
            	}
            }
        })
    });
});