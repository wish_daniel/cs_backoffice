//variable setting
var url_mainserver = "http://wishtrend.com/wish_cs_API2/search.php";
var url_testserver = "http://wishdevelop.com/wish_cs_API/search.php";

// this code for testcode's js
        $('#submit').click(function(){            
            $.ajax({
                // url:'http://wishtrend.com/wish_cs_API2/search.php',
                url:'http://wishdevelop.com/wish_cs_API/search.php',
                type:'GET',
                dataType:'jsonp',
                crossDomain:true,
                data:{
                    'InputEmail':$('#InputEmail').val(),
                    'InputOrderRefText':$('#InputOrderRefText').val(),
                    'InputOrderRefNum':$('#InputOrderRefNum').val(),
                    'InputTransID':$('#InputTransID').val(),                    
                    },                
                success:function(data){
                    var str = data;
                    console.log(data);
                    //reset                                        
                    $('#UserTable > tbody').html('');
                    $('#UserHDTable > tbody').html('');
                    $('#ProductTable > tbody').html('');
                    $('#AddressTable > tbody').html('');   
                    $('#TotalTable > tbody').html('');
                    $('#DiscountTable > tbody').html('');
                    $('#History > tbody').html('');
                    $('#CSTable > tbody').html('');
                    $('#CSHDTable > tbody').html('');
                    $('#active').html('');
                    
                    //CS 내역 노출
                    bindCSTable(str[0][0]);
                    $('#cs_user_email').val(str[0][0]);

                    //email 표
                    $('#td_email').html(str[0][0]);                                                                                                                  
                    
                    //주문내역 더 없을경우 하단 moreorder 버튼 안뜨도록 설정
                    var email_tmp = $('#InputEmail').val();
                    
                    if(str[1][2] == null || email_tmp == '') {
                        $('#Orderbutton').css('visibility','hidden');
                    } else {
                        $('#Orderbutton').css('visibility','visible');
                    }

                    //사용자 정보 출력                    
                    if(str[4]==2) {
                        $n=0;
                        while($n<2){
                            if(str[1][$n]!=null){
                                $('#UserTable > tbody:last').append('<tr style="border:1px gray solid;"><td align="center" id="td_IdOrder_'+$n+'"><a class="no-uline">'+str[1][$n].id_order+'</a></td>'
                                +'<td align="center" id="td_Reference_'+$n+'">'+str[1][$n].reference+'</td>'
                                +'<td align="center">'+str[1][$n].date_add+'</td>'
                                +'<td align="center">'+str[1][$n].delivery_date+'</td>'
                                +'<td align="center"><a href="'+str[1][$n][7]+'" target="_blank">'+str[1][$n].shipping_number+'</a></td>'
                                +'<td align="center"><a id="cancellbtn_'+$n+'" class="btn btn-primary">Cancell</a></td></tr>');
                            }
                            $n++;
                        }                                                                        
                        
                        $n=2;
                        while(str[1]!=null && str[1][$n]!=null){
                            $('#UserHDTable > tbody:last').append('<tr style="border:1px gray solid;"><td align="center" id="td_IdOrder_'+$n+'"><a class="no-uline">'+str[1][$n].id_order+'</a></td>'
                            +'<td align="center" id="td_Reference_'+$n+'">'+str[1][$n].reference+'</td>'
                            +'<td align="center">'+str[1][$n].date_add+'</td>'
                            +'<td align="center">'+str[1][$n].delivery_date+'</td>'
                            +'<td align="center"><a href="'+str[1][$n][7]+'" target="_blank">'+str[1][$n].shipping_number+'</a></td>'
                            +'<td align="center"><a id="cancellbtn_'+$n+'" class="btn btn-primary">Cancell</a></td></tr>');
                            $n++;
                        }                        


                        $n=0;                        
                        while(str[5]!=null && str[5][$n]!=null){                            
                            $('#ProductTable > tbody:last').append('<tr style="border:1px gray solid;"><td>'+str[5][$n].product_name+'</td>'
                            +'<td align="center">'+str[5][$n].product_quantity+'</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].unit_price_tax_incl*100))/100+'$</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].total_price_tax_incl*100))/100+'$</td></tr>');
                            $n++;
                        }                        
                        $('#ProductTable > tbody:last').append('<tr><td></td><td></td><td align="center">TotalPrice</td><td align="center">'+(Math.round(str[8][0]*100))/100+'$</td></tr>');
                        
                        $('#td_OrderRefNum_1').html(+str[1][0].id_order);                                        
                        $('#td_OrderRefText_1').html(str[1][0].reference);
                        $('#td_transactionID').html(str[3].transaction_id);
                        $('#AddressTable > tbody').append('<tr><td style="margin-left:-10px;">'                        
                        +'Name : '
                        +' <div class="DI" id="firstname_id">'+str[6].firstname+'</div>'
                        +' <div class="DI" id="lastname_id">'+str[6].lastname+'</div> /'
                        +'  Company : '
                        +' <div class="DI">'+str[6].company+'</div> /'
                        +'  Address : '
                        +' <div id="address_id_1" class="DI">'+str[6].address1+'</div>'
                        +' <div id="address_id_2" class="DI">'+str[6].address2+'</div> /'
                        +'  Postcode : '
                        +' <div id="postcode_id" class="DI">'+str[6].postcode+'</div> /'
                        +'  City : '
                        +' <div id="city_id_1" class="DI">'+str[6].city+'</div> /'
                        +'  Country : '
                        +' <div id="country_id_1" class="DI">'+str[6].country+'</div> /'
                        +'  Phone : '
                        +' <div id="phone_id_1" class="DI">'+str[6].phone+'</div> /'
                        +'  Mobile : '
                        +' <div id="phone_mobile_id_1" class="DI">'+str[6].phone_mobile+'</div> /'
                        +'<div id="id_address" class="DIhide">'+str[6].id+'</div>'
                        +'</td></tr>');
                        
                        $('#DiscountTable > tbody').append('<tr><td>'
                        +str[7][0]+'</td>'
                        +'<td>'+(Math.round(str[8][2]*100))/100+'$</td>'
                        +'<td>'+(Math.round(str[8][3]*100))/100+'$</td></tr>');
                        
                        $('#TotalTable > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
                        +(Math.round(str[8][1]*100))/100+'$</td></tr>');                                                
                        bindTableEvent();
                        BindCancellEvent();
                        //end add line
                        
                    } else if(str[4]==1) {
                        $('#UserTable > tbody:last').append('<tr><td align="center" id="td_IdOrder_0"><a class="no-uline">'+str[1].id_order+'</a></td>'
                            +'<td align="center">'+str[1].reference+'</td>'
                            +'<td align="center">'+str[1].date_add+'</td>'
                            +'<td align="center">'+str[1].delivery_date+'</td>'
                            +'<td align="center"><a href="'+str[9]+'" target="_blank">'+str[1].shipping_number+'</a></td>'
                            +'<td align="center"><a id="cancellbtn_0" class="btn btn-primary">Cancell</a></td></tr>');
                        
                        $n=0;
                        while(str[5]!=null && str[5][$n]!=null){                            
                            $('#ProductTable > tbody:last').append('<tr style="border:1px gray solid;"><td>'+str[5][$n].product_name+'</td>'
                            +'<td align="center">'+str[5][$n].product_quantity+'</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].unit_price_tax_incl*100))/100+'$</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].total_price_tax_incl*100))/100+'$</td></tr>');                                                                                             
                            $n++;
                        }                        
                        
                        $('#ProductTable > tbody:last').append('<tr><td></td><td></td><td align="center">TotalPrice</td><td align="center">'+(Math.round(str[8][0]*100))/100+'$</td></tr>');
                        $('#td_OrderRefNum_1').html(str[1].id_order);                                        
                        $('#td_OrderRefText_1').html(str[1].reference);
                        $('#td_transactionID').html(str[3].transaction_id);
                        $('#AddressTable > tbody').append('<tr><td>'
                        +'Name : '
                        +' <div class="DI" id="firstname_id">'+str[6].firstname+'</div>'
                        +' <div class="DI" id="lastname_id">'+str[6].lastname+'</div> /'
                        +'  Company : '
                        +' <div class="DI">'+str[6].company+'</div> /'
                        +'  Address : '
                        +' <div id="address_id_1" class="DI">'+str[6].address1+'</div>'
                        +' <div id="address_id_2" class="DI">'+str[6].address2+'</div> /'
                        +'  Postcode : '
                        +' <div id="postcode_id" class="DI">'+str[6].postcode+'</div> /'
                        +'  City : '
                        +' <div id="city_id_1" class="DI">'+str[6].city+'</div> /'
                        +'  Country : '
                        +' <div id="country_id_1" class="DI">'+str[6].country+'</div> /'
                        +'  Phone : '
                        +' <div id="phone_id_1" class="DI">'+str[6].phone+'</div> /'
                        +'  Mobile : '
                        +' <div id="phone_mobile_id_1" class="DI">'+str[6].phone_mobile+'</div> /'
                        +'<div id="id_address" class="DIhide">'+str[6].id+'</div>'
                        +'</td></tr>');
                        
                        $('#DiscountTable > tbody').append('<tr><td>'
                        +str[7][0]+'</td>'
                        +'<td>'+(Math.round(str[8][2]*100))/100+'$</td>'
                        +'<td>'+(Math.round(str[8][3]*100))/100+'$</td></tr>');
                        
                        $('#TotalTable > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
                        +(Math.round(str[8][1]*100))/100+'$</td></tr>');
                        
                        $n=0;
                        while(str[10]!=null && str[10][$n]!=null){
	                        $('#History > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
	                        +str[10][$n].ostate_name+'</td>'                       
	                        +'<td align="center">'+str[10][$n].employee_firstname+'</td>'
	                        +'<td align="center">'+str[10][$n].employee_lastname+'</td>'
	                        +'<td align="center">'+str[10][$n].date_add+'</td>'+
	                        +'</tr>');
	                        $n++;
                        }
                        bindTableEvent();
                        BindCancellEvent();
                    }                                                                                                           
                                                           
                    if(str[2].active==1){
                        $('#active').html("activated");
                    } else {
                        $('#active').html("deactivated");
  	                }		                   
  	                
  	                $('#InputOrderRefText').val('');
  	                $('#InputOrderRefNum').val('');
  	                $('#InputEmail').val('');
  	                $('#InputTransID').val('');                     
                }                
            })
        });
                
        //만약 사용자의 상태가 deactivated 일 경우 activate로 변환시켜준다.        
        $('#active').click(function() {
            if($('#active').text() == 'deactivated') {
                $.ajax({
                    // url:'http://wishtrend.com/wish_cs_API2/activate.php',
                    url:'http://wishdevelop.com/wish_cs_API/activate.php',
                    type:'GET',
                    dataType:'jsonp',
                    crossDomain:true,
                    data:{
                        'InputEmail':$('#InputEmail').val()
                        },
                    success:function(data){                        
                        if(data[0]==true)
                            $('#active').html('activated');           
                    },
                    error:function(data){
                        if(data[0]==false)
                            console.log('active-error');                        
                    }                    
                })
            }
        });
        
       $(document).ready(function(){ 
            $('#inputs').change(function(e){
                console.log('input');
                var txt = $('#InputOrderRefText').val();
                var num = $('#InputOrderRefNum').val();
                var tran = $('#InputTransID').val();
                var email = $('#InputEmail').val();
                
               if(txt != ''){                   
                   $('#InputOrderRefText').html('');
               } else if(num != '') {
                   $('#InputOrderRefNum').html('');
               } else if(email != '') {
                   $('#InputEmail').html('');
               } else if(tran != '') {
                   $('#InputTransID').html('');                   
               } else {
                       
               }
                              
            });
        });  
        
        
    //Manual Start
    $(document).ready(function(){ 
    	$.ajax({
                    url:'http://wishtrends.net/~wish_cs/view/cs_start.php',
                    type:'POST',
                    dataType:'jsonp',
                    crossDomain:true,                    
                    success:function(data){
                    	console.log(data);
                    	$('#notice_main').html(data[0]['notice']);  
                        }
                })
    	
        $('#Orderbutton').css('visibility','hidden');

            //Manual control
            $('#edit').click(function(e){
                $('#edit_text').css("visibility","visible");
                var str = $('#notice_main').html();
                var change_str = str.replace(/\<br\\?>/g, "\n");
                console.log(change_str);
                $('#edit_text').html(change_str);
            });   
                        
            $('#save').click(function(e){
                $('#edit_text').css("visibility","hidden");
                var textarea_a = $('#edit_text').val();
                var chk = "manual";                

                $.ajax({
                    url:'http://wishtrends.net/~wish_cs/controller/db_control.php',                    
                    type:'POST',
                    dataType:'jsonp',
                    crossDomain:true,
                    data:{                                                                        
                        'textarea':textarea_a,                                                 
                        'chk':chk,                        
                        },
                    success:function(data){
                        $('#notice_main').html(data[0]['notice']);                                              
                        }
                })
            })


            //user's cs info control
            $('#edit_info_button').click(function(e){
                $('#edit_info').css("visibility","visible");
                $('#edit_info').html($('#user_info_main').html());                
            });
            
            $('#save_info_button').click(function(e){
                $('#edit_info').css("visibility","hidden");
                var textarea_a = $('#edit_info').val();                                                        
                var chk = "user";
                var email = $('#td_email').html();

                console.log(email);

                //ajax
                $.ajax({
                    url:'http://wishtrends.net/~wish_cs/controller/db_control.php',                    
                    type:'POST',
                    dataType:'jsonp',
                    crossDomain:true,
                    data:{                                                                        
                        'textarea':textarea_a,
                        'chk':chk,
                        'email':email,
                        },
                    success:function(data){
                    	// $('#user_info_main').html(data[0]['user_cs']);                    	                        
                        }
                })
               })
            });                       
 
// Add CS History
$(document).ready(function(){ 
    $('#save_cs').click(function(e){
        var chk = 'insert';
        var email = $('#cs_user_email').val();
        var content = $('#cs_content').val();
        var user_id = $('#user_id_top').html();

        $('#cs_content').val('');

        $.ajax({
            url:'http://wishtrends.net/~wish_cs/controller/db_control.php',
            type:'POST',
            dataType:'jsonp',
            crossDomain:true,
            data:{
                'chk':chk,
                'email':email,
                'content':content,
                'user_id':user_id,
            },
            success:function(data){
                $('#CSTable > tbody').html('');
                $('#CSHDTable > tbody').html('');
                var user_cs = data;

                if(user_cs[0][2] == null)
                {
                    $('#CSbutton').css('visibility','hidden');
                    $('#Orderbutton').css('visibility','hidden');
                } else {
                    $('#CSbutton').css('visibility','visible');
                }

                $n=0;                        
                while($n != 2){                            
                    $('#CSTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;
                }

                //hidden CS table control
                $n=2;                        
                while(user_cs[0]!=null && user_cs[0][$n]!=null){                            
                    $('#CSHDTable > tbody:last').append(
                    '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                    +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                    );
                $n++;
                }
            }
        })
    });
});



// change user's address on click modal's save button 
$(document).ready(function(){ 
    $('#save_address_modal').click(function(e){
        var email = $('#td_email').html();
        var chk = 'address';
        var firstname = $('#firstname_id').html();
        var lastname = $('#lastname_id').html();
console.log('#firstname_id');
        var address1 = $('#change_address_1').val();
        var address2 = $('#change_address_2').val();
        var postcode = $('#change_postcode').val();
        var city = $('#change_city').val();
        var phone = $('#change_phone').val();
        var phone_mobile = $('#change_phone_mobile').val();
        var id_address = $('#id_address').html();
        $('#ErrorDisplay').html('');

        $.ajax({
            url:'http://wishdevelop.com/wish_cs_API/change_address.php',
            // url:'http://wishtrend.com/wish_cs_API2/change_address.php',
            type:'POST',
            dataType:'jsonp',
            crossDomain:true,
            data:{
                'email':email,
                'chk':chk,
                'firstname':firstname,
                'lastname':lastname,
                'address1':address1,
                'address2':address2,
                'postcode':postcode,
                'city':city,                
                'phone': phone,
                'phone_mobile': phone_mobile,
                'id_address': id_address,
            },
            success:function(data){
                console.log(data);
                    //error check
                    if(data[0] == 'false')
                    {
                        //Error message display
                        if(data[1] == 1)
                        {
                            console.log('in_1');
                            $('#ErrorDisplay').append('<div id="ErrorDisplayInner">'+
                                data[2]+data[3]+'</div>');                                     
                        }
                        else if(data[1] == 2)
                        {
                            $('#ErrorDisplay').append('<div id="ErrorDisplayInner">'+
                                data[2]+'</div>');                                     
                        }
                        else if(data[1] == 3)
                        {
                            $('#ErrorDisplay').append('<div id="ErrorDisplayInner">'+
                                data[2]+'</div>');                                                    
                        }    
                    } 
                    else if(data[0] == 'true')
                    {
                        $('#addresspop').modal('hide');
                    }
            }
        })
    });
});

//postcode checker
$(document).ready(function(){
    $('#change_postcode').keyup(function(e){
        var postcode = $('#change_postcode').val();
        var email = $('#td_email').html();;

        console.log(email);

        if(postcode != ''){
            console.log(postcode);
            $.ajax({
                url:'http://wishdevelop.com/wish_cs_API/change_address.php',
                // url:'http://wishtrend.com/wish_cs_API2/change_address.php',
                type:'POST',
                dataType:'jsonp',
                crossDomain:true,
                data:{
                    'postcode':postcode,
                    'email': email,
                },
                success:function(data){

                    console.log(data);
                    //Postcode Checker
                    if(data[0] == 'true'){
                        $('#change_postcode').css('background-color','white');
                    }
                    else if(data[0] == 'false')
                    {
                        $('#change_postcode').css('background-color','lightcoral');
                    }

                }
            })
        }        
    });

    //click modal's close button
    $('#close_address_modal').click(function(e){
        $('#change_postcode').css('background-color','white');
    });
});

//CS List 가져오기        
function bindCSTable(email){
        var chk = "load";

        $.ajax({
            url:'http://wishtrends.net/~wish_cs/controller/db_control.php',
            type:'POST',
            dataType:'jsonp',
            crossDomain:true,
            data:{                        
                'chk':chk,
                'email':email,
                },
            success:function(data){
                    //to do
                    $('#CSHDTable > tbody').html('');
                    var user_cs = data;

                    if(user_cs[0][2] == null)
                    {
                        $('#CSbutton').css('visibility','hidden');
                    }
                    else {
                        $('#CSbutton').css('visibility','visible');
                    }                    

                    $n=0;                        
                    while($n != 2){               
                        if(user_cs[0][$n]!=null){             
                            $('#CSTable > tbody:last').append(
                            '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                            +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                            );
                        }
                    $n++;
                    }

                    //hidden CS table control
                    $n=2;                        
                    while(user_cs[0]!=null && user_cs[0][$n]!=null){                            
                        $('#CSHDTable > tbody:last').append(
                        '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].enroll_date+'</td>'                        
                        +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'                        
                        );
                    $n++;
                    }
                }
        })
}        

//cancellation jqeury
function BindCancellEvent() {
    $('#UserTable tr > td:nth-child(6) > a, #UserHDTable tr > td:nth-child(6) > a').on('click',(function(e){        
        //view log
        var order_num = $(e.currentTarget).parent().prev().prev().prev().prev().prev().children().html();
        var order_text = $(e.currentTarget).parent().prev().prev().prev().prev().html();
        var email = $('#td_email').html();


        $.ajax({
                    //main server
                    //url:'http://wishtrend.com/wish_cs_API2/cancell.php',
                    //test server
                    url:'http://wishdevelop.com/wish_cs_API/cancell.php',
                    type:'GET',
                    dataType:'jsonp',
                    crossDomain:true,
                    data:{
                        'order_num':order_num,
                        'order_text':order_text,
                        'email':email,
                        },
                    success:function(data){
                        console.log(data);
                        var str = data;
                        if(str[0] == 0)
                        {
                            console.log('cancell failed');
                            alert("It's not available for this status");
                        }
                        else if (str[0] == 1)
                        {
                            console.log('cancell success');
                            alert("Cancell success");

                            $('#History > tbody').html('');

                            $n=0;
                            while(str[1]!=null && str[1][$n]!=null){
                                $('#History > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
                                +str[1][$n].ostate_name+'</td>'
                                +'<td align="center">'+str[1][$n].employee_firstname+'</td>'
                                +'<td align="center">'+str[1][$n].employee_lastname+'</td>'
                                +'<td align="center">'+str[1][$n].date_add
                                +'</td></tr>');
                            $n++;
                            }
                            //call DB insert data function at wishtrends.net DB
                            InsertCancellData(str[2][0],str[2][1],str[2][2],str[2][3],str[2][4]);
                        }
                    }
                })
    }));
}

function InsertCancellData(email,order_num,order_text,trance_id,total_price) {
    $.ajax({
            url:'http://wishtrends.net/~wish_cs/controller/db_cancell_insert.php',
            type:'GET',
            dataType:'jsonp',
            crossDomain:true,
            data:{                                                                        
                'email':email,
                'order_num':order_num,
                'order_text':order_text,
                'trance_id':trance_id,
                'total_price':total_price,
                },
            success:function(data){
                console.log(data);
                var str = data;
                if(str[0] == 1)
                {
                    console.log('cancell infomation insert to Database');
                }                        
            }
    })
}

//UserTable의 값들 중 첫번째 td값들만 선택이 가능하도록
function bindTableEvent() {
	// $('#UserTable tr > td:nth-child(1), #UserTable tr > td:nth-child(2)').on('click',function(e){
    $('#UserTable tr > td:nth-child(1), #UserHDTable tr > td:nth-child(1)').on('click',function(e){                
                var txt = $('#InputOrderRefText').val();
                var num = $('#InputOrderRefNum').val();
                var email = $('#InputEmail').val();
                
              if(txt == '' && num == ''){
                //var id = $(e.currentTarget).parent().attr('id');
                var InputOrderRefNum = $(e.currentTarget).text();
                
                if (InputOrderRefNum.length!=0){  
	                var is_reference = parseInt(InputOrderRefNum)>0?false:true;            
                }
                
                //ajax
                $.ajax({
                    // url:'http://wishtrend.com/wish_cs_API2/search.php',
                    url:'http://wishdevelop.com/wish_cs_API/search.php',
                    type:'GET',
                    dataType:'jsonp',
                    crossDomain:true,
                    data:{                                                                        
                        'InputOrderRefNum':InputOrderRefNum,
                        'InputEmail':'',
                        'InputOrderRefText':'',
                        'InputTransID':'',                           
                        },
                    success:function(data){
                        console.log(data);
                        
                        var str = data;
                        console.log(data);
                        //reset                                                                
                        $('#ProductTable > tbody').html('');
                        $('#AddressTable > tbody').html('');   
                        $('#TotalTable > tbody').html('');
                        $('#DiscountTable > tbody').html('');  
                        $('#History > tbody').html('');                    
                                                                        
                        
                        $n=0;                        
                        while(str[5]!=null && str[5][$n]!=null){                            
                            $('#ProductTable > tbody:last').append('<tr style="border:1px gray solid;"><td>'+str[5][$n].product_name+'</td>'
                            +'<td align="center">'+str[5][$n].product_quantity+'</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].unit_price_tax_incl*100))/100+'$</td>'
                            +'<td align="center">'+(Math.round(str[5][$n].total_price_tax_incl*100))/100+'$</td></tr>');                                                                                             
                            $n++;
                        }                        
                        
                        $('#ProductTable > tbody:last').append('<tr><td></td><td></td><td align="center">TotalPrice</td><td align="center">'+(Math.round(str[8][0]*100))/100+'$</td></tr>');                     
                        
                        $('#td_OrderRefNum_1').html(str[1].id_order);                                        
                        $('#td_OrderRefText_1').html(str[1].reference);
                        $('#td_transactionID').html(str[3].transaction_id);
                        $('#AddressTable > tbody').append('<tr><td>'
                        +'Name : '
                        +' <div class="DI" id="firstname_id">'+str[6].firstname+'</div>'
                        +' <div class="DI" id="lastname_id">'+str[6].lastname+'</div> /'
                        +'  Company : '
                        +' <div class="DI">'+str[6].company+'</div> /'
                        +'  Address : '
                        +' <div id="address_id_1" class="DI">'+str[6].address1+'</div>'
                        +' <div id="address_id_2" class="DI">'+str[6].address2+'</div> /'
                        +'  Postcode : '
                        +' <div id="postcode_id" class="DI">'+str[6].postcode+'</div> /'
                        +'  City : '
                        +' <div id="city_id_1" class="DI">'+str[6].city+'</div> /'
                        +'  Country : '
                        +' <div id="country_id_1" class="DI">'+str[6].country+'</div> /'
                        +'  Phone : '
                        +' <div id="phone_id_1" class="DI">'+str[6].phone+'</div> /'
                        +'  Mobile : '
                        +' <div id="phone_mobile_id_1" class="DI">'+str[6].phone_mobile+'</div> /'
                        +'<div id="id_address" class="DIhide">'+str[6].id+'</div>'
                        +'</td></tr>');                   

                        $('#DiscountTable > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
                        +str[7][0]+'</td>'
                        +'<td align="center">'+(Math.round(str[8][2]*100))/100+'$</td>'
                        +'<td align="center">'+(Math.round(str[8][3]*100))/100+'$</td></tr>');
                        
                        $('#TotalTable > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
                        +(Math.round(str[8][1]*100))/100+'$</td></tr>');
                        
                        //History
                        $n=0;
                        while(str[10]!=null && str[10][$n]!=null){
	                        $('#History > tbody').append('<tr style="border:1px gray solid;"><td align="center">'
	                        +str[10][$n].ostate_name+'</td>'                       
	                        +'<td align="center">'+str[10][$n].employee_firstname+'</td>'
	                        +'<td align="center">'+str[10][$n].employee_lastname+'</td>'
	                        +'<td align="center">'+str[10][$n].date_add                                           
	                        +'</td></tr>');
	                        $n++;
                        }
                    }
                })
                }
            });
}  