
//only last example using format display
$(function () {
    $('.format-date').each(function () {
        var $display = $('.date-display', this);
        $(this).on('change', function (e) {
            //webshim.format will automatically format date to according to webshim.activeLang or the browsers locale
            var localizedDate = webshim.format.date($.prop(e.target, 'value'));
            $display.html(localizedDate);
        });
    });
});

$('#CalBtn').click(function() {
    var Sdate = $('#Sdate').val();
    var Edate = $('#Edate').val();
    
    var type="calsearch"; 

    $.ajax({
        url:'http://wishtrends.net/~wish_cs/controller/db_call_CSList.php',
        type:'POST',
        dataType:'jsonp',
        data:{
            'type': type,
            'Sdate': Sdate,
            'Edate': Edate,
        },
        crossDomain:true,           
        success:function(data){
            console.log(data);
            $('#IdCsListTable > tbody').html('');            
            var user_cs = data;            

            $n=0;                        
            while(user_cs[0]!=null){                            
                $('#IdCsListTable > tbody:last').append(
                '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].cs_key+'</td>'                        
                +'<td align="center">'+user_cs[0][$n].employee+'</td>'
                +'<td align="center">'+user_cs[0][$n].email+'</td>'
                +'<td align="center">'+user_cs[0][$n].enroll_date+'</td>'
                +'<td align="center">'+user_cs[0][$n].type+'</td>'
                +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'
                );
            $n++;
            }          
        }
    })
});

$('#CalBtnPartner').click(function() {
    var Sdate = $('#SdatePartner').val();
    var Edate = $('#EdatePartner').val();
    
    var type="calsearch"; 
    
    $.ajax({
        url:'http://wishtrends.net/~wish_cs/controller/db_call_alliance_CSList.php',
        type:'POST',
        dataType:'jsonp',
        data:{
            'type': type,
            'Sdate': Sdate,
            'Edate': Edate,
        },
        crossDomain:true,           
        success:function(data){
			console.log(data);
            $('#IdCsListTablePartner > tbody').html('');            
            var user_cs = data;            

            $n=0;                        
            while(user_cs[0]!=null){                            
                $('#IdCsListTablePartner > tbody:last').append(
                '<tr style="border:1px gray solid;"><td align="center">'+user_cs[0][$n].alliance_cs_key+'</td>'                        
                +'<td align="center">'+user_cs[0][$n].employee+'</td>'
                +'<td align="center">'+user_cs[0][$n].email+'</td>'
                +'<td align="center">'+user_cs[0][$n].enroll_date+'</td>'
                +'<td align="center">'+user_cs[0][$n].type+'</td>'
                +'<td align="center">'+user_cs[0][$n].content+'</td></tr>'
                );
            $n++;
            }          
        }
    })
});