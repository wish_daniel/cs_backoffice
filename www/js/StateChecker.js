//change state
$('#CancelStateSaveBtn').click(function(e){ 
	
    var chk_list = new Array();
    var state = $('#StateSelector').val();

    var chk_num = 0
    while($('#cancel_chk_'+chk_num).val() != null)
    {
        //get Cancel Index
        var cancel_idx = $('#cancel_chk_'+chk_num).parent().parent().parent().next().html();

        //make idx list
        var chk = $('input:checkbox[id="cancel_chk_'+chk_num+'"]').is(":checked") == true;
        if(chk == true)
            chk_list.push(cancel_idx);
        
        chk_num++;
    }
    
    $.ajax({
        url:'http://wishtrends.net/~wish_cs/controller/db_call_cancel_state_change.php',
        type:'POST',
        dataType:'jsonp',
        data:{
            'state':state,
            chk_list:chk_list,
        },
        success:function(data){
            console.log(data);
            $('#IdCancellTable > tbody').html('');
            var cancell_list = data;      
            
            $n=0;                        
            while(cancell_list[0]!=null){
                $('#IdCancellTable > tbody:last').append(
                '<tr style="border:1px gray solid;"><td align="center" class="table_middle"><div class="checkbox"><label><input id="cancel_chk_'+$n+'" class="cancel_chk_class" type="checkbox"></label></div></td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].cancell_key+'</td>'                        
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].enroll_date+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].state+'</td>'                
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].email+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].order_text+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].order_num+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].trance_id+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].total_price+'$</td></tr>'
                );
            $n++;
            } 
        }
    });
});

//change state category
$('#StateSelectorTop').change(function(e){ 
    
    var state = $('#StateSelectorTop').val();
    
    $.ajax({
        url:'http://wishtrends.net/~wish_cs/controller/db_call_cancel_state.php',
        type:'POST',
        dataType:'jsonp',
        data:{
            'state':state,            
        },
        success:function(data){
            console.log(data);
            $('#IdCancellTable > tbody').html('');
            var cancell_list = data;      
            
            $n=0;                        
            while(cancell_list[0]!=null){
                $('#IdCancellTable > tbody:last').append(
                '<tr style="border:1px gray solid;"><td align="center" class="table_middle"><div class="checkbox"><label><input id="cancel_chk_'+$n+'" class="cancel_chk_class" type="checkbox"></label></div></td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].cancell_key+'</td>'                        
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].enroll_date+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].state+'</td>'                
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].email+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].order_text+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].order_num+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].trance_id+'</td>'
                +'<td align="center" class="table_middle">'+cancell_list[0][$n].total_price+'$</td></tr>'
                );
            $n++;
            } 
        }
    });
});